---
food: pizza
fruits :
  - apples
  - pears
  - cherries
mapage: sandbox
---

# Accueil Général - Test Page

## Maths 2 { #title-id }

$\displaystyle x = \sum_{i=0}^{+\infty} \frac 1{i^2}$ et $y= \sqrt 2$ et $\displaystyle z = \frac {\frac 23}{\frac 37}$

$$
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
$$

{​–deleted–} and replacement text {​++added++}

This can also be combined into {​one~>a single} operation. {​==Highlighting==} is also possible {​>>and comments can be added inline<<}.

{​==

Formatting can also be applied to blocks, by putting the opening and closing tags on separate lines and adding new lines between the tags and the content.

==}

## Mermaid

```mermaid
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
B --> E[Server03]
```

## Graphviz

TEST 1 :

```graphviz dot un.png
digraph G {
    rankdir=LR
    TerreSandbox [peripheries=2]
    Mars
    TerreSandbox -> Mars
}
```

TEST 2:

```graphviz dot deux.png
graph G {
    node [shape=circle]
    a -- b
}
```

## Strikethrough and Subscript

This text ~is\ superscript~ while this one is ~~striked out~~

## TasksLists

- [X] Write this documentation
- [ ] Check the website
- [ ] Contact the media

## Footnotes

Here is a simple footnote,[^1] and here is a longer one.[^bignote]

[^1]: This is the first footnote.

[^bignote]: Here is one with multiple paragraphs and code.

    Indent paragraphs to include them in the footnote.

    `{ my code }`

    Add as many paragraphs as you like.

Got [Maths Id](#title-id)

## Tabs


=== "Image"

    ```yaml
    theme:
        logo: assets/logo.png
    ```

=== "Icon, bundled"

    ```yaml
    theme:
        icon:
        logo: material/library
    ```

=== "Hello"

    dfq


## Admonitions 

!!! success "Printing numbers"

    === "Image"

        ```yaml
        theme:
          logo: assets/logo.png
        ```

    === "Icon, bundled"

        ```yaml
        theme:
          icon:
            logo: material/library
        ```

    === "Hello"

        dfq

## Icons & Emojis

### Material

:material-check:  :material-check-all:   :material-close:

### Octicons

:octicons-file-code-24: Source · :octicons-milestone-24: Default: material/library

### Emojis

The logo can be changed to a user-provided image (any type, incl. *.png and *.svg) located in the docs folder, or to any icon bundled with the theme. Add the following lines to mkdocs.yml:

:smile: :grinning: :+1:

## Buttons

[Subscribe to our mailing list](https://www.google.com){ .md-button }

![A Tux](src/img/tux.gif){ align=right }

Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla 
Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla 
Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla 
Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla 




## Floating Paragraph

!!! bug inline end "Sidebar"

    This text is in a floating paragraph, which will appear to the right of the
    normal text, or full width on mobile screens.

Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla
Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla
Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla
Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla
Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla
Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla
Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla
Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla
Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla Blabla



## Collapsible Blocks

??? note "Block closed by default"

    The text is inside a collapsible block.

???+ note "Block open by default:"

    The text $\sqrt 2$ is inside a collapsible block.

???+ note "Block with graphviz"

    ```graphviz dot test.svg
    digraph G {
        rankdir=LR
        TerreSandbox [peripheries=2]
        Mars
        TerreSandbox -> Mars
    }
    ```

```dot
digraph G {
    rankdir=LR
    TerreSandbox [peripheries=2]
    Mars
    TerreSandbox -> Mars
}
```

???+ note "Block with mermaid"

    ```mermaid
    graph TD
    A[Client] --> B[Load Balancer]
    B --> C[Server01]
    B --> D[Server02]
    B --> E[Server03]
    ```

```mermaid
graph TD
A[Client] --> B[Load Balancer]
B --> C[Server01]
B --> D[Server02]
B --> E[Server03]
```


## Comments

The table length if obtained by `:::bash ${{ "{#" }}table[@]}`.

{% raw %}
The string length of the element `i` is `:::bash ${#table[i]}`.

``` bash
if test ${#foo} -ge 5; then
    echo "ok"
fi
```
{% endraw %}

## Meta Variables

### Food

#### Yes

Il like eating a {{ page.meta.food }}.

#### No

qsdf

### Fruits

The fruits are: {% for f in fruits %} {{ f }}{% endfor %}.

## Global Variables


{% import "src/includes/mydefs.html" as my %}

The short link to this site is <{{ my.short_url }}>.

## TOC

[TOC]

TOC via Jinja:

{% for page in navigation.pages %}
- [{{ page.title }}](../{{ page.url }}){% endfor %}


