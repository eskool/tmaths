import math;

size(6cm,0);

add(grid(6,5,lightgray));

pair pA=(1,1), pB=(5,1), pC=(4,4);

draw(pA--pB--pC--cycle);
dot("$A$",pA,dir(pC--pA,pB--pA));
dot("$B$",pB,dir(pC--pB,pA--pB));
dot("$C$",pC,dir(pA--pC,pB--pC));
